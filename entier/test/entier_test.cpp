#include "entier/entier.hpp"
// include Google Mock and iostream
#include <gmock/gmock.h>

#include <cmath>
#include <iostream>
#include <stdexcept>

namespace nombres {
namespace {

using testing::Eq;

// testing the object creation
TEST(Entier, creation) {
  try {
    Entier test = Entier(1);
    Entier test2 = Entier();
    SUCCEED();
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality1) {
  try {
    Entier test = Entier(1);
    Entier test2 = Entier(1);
    ASSERT_THAT(test == test2, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality2) {
  try {
    Entier test = Entier(10000000);
    Entier test2 = Entier(10000000);
    ASSERT_THAT(test == test2, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality3) {
  try {
    Entier test = Entier(451);
    Entier test2 = Entier(-451);
    ASSERT_THAT(test == test2, Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality4) {
  try {
    Entier test = Entier(-456789);
    Entier test2 = Entier(-456789);
    ASSERT_THAT(test == test2, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality5) {
  try {
    Entier test = Entier(145555);
    Entier test2 = Entier(145555);
    ASSERT_THAT(test == test2, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality6) {
  try {
    Entier test = Entier(145555);
    ASSERT_THAT(test == 145555, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality7) {
  try {
    Entier test = Entier(-5555);
    ASSERT_THAT(test == -5555, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, equality8) {
  try {
    Entier test = Entier(0);
    ASSERT_THAT(test == 0, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLeq1) {
  try {
    ASSERT_THAT(Entier(2)<=Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLeq2) {
  try {
    ASSERT_THAT(Entier(1)<=Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLeq3) {
  try {
    ASSERT_THAT(Entier(3)<=Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLeq4) {
  try {
    ASSERT_THAT(Entier(-1)<=Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLeq5) {
  try {
    ASSERT_THAT(Entier(1)<=Entier(-2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLeq6) {
  try {
    ASSERT_THAT(Entier(-1)<=Entier(-2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLes1) {
  try {
    ASSERT_THAT(Entier(2)<Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLes2) {
  try {
    ASSERT_THAT(Entier(1)<Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLes3) {
  try {
    ASSERT_THAT(Entier(3)<Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLes4) {
  try {
    ASSERT_THAT(Entier(-1)<Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLes5) {
  try {
    ASSERT_THAT(Entier(1)<Entier(-2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareLes6) {
  try {
    ASSERT_THAT(Entier(-1)<Entier(-2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGeq1) {
  try {
    ASSERT_THAT(Entier(2)>=Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGeq2) {
  try {
    ASSERT_THAT(Entier(1)>=Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGeq3) {
  try {
    ASSERT_THAT(Entier(3)>=Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGeq4) {
  try {
    ASSERT_THAT(Entier(-1)>=Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGeq5) {
  try {
    ASSERT_THAT(Entier(1)>=Entier(-2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGeq6) {
  try {
    ASSERT_THAT(Entier(-1)>=Entier(-2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGes1) {
  try {
    ASSERT_THAT(Entier(2)>Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGes2) {
  try {
    ASSERT_THAT(Entier(1)>Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGes3) {
  try {
    ASSERT_THAT(Entier(3)>Entier(2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGes4) {
  try {
    ASSERT_THAT(Entier(-1)>Entier(2), Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGes5) {
  try {
    ASSERT_THAT(Entier(1)>Entier(-2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, compareGes6) {
  try {
    ASSERT_THAT(Entier(-1)>Entier(-2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add1) {
  try {
    Entier test = Entier(10);
    Entier test2 = Entier(10);
    Entier test3 = Entier(20);
    ASSERT_THAT((test + test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add2) {
  try {
    Entier test = Entier(10000000);
    Entier test2 = Entier(10000000);
    Entier test3 = Entier(20000000);
    ASSERT_THAT((test + test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add3) {
  try {
    Entier test = Entier(10000000);
    Entier test2 = Entier(0);
    Entier test3 = Entier(10000000);
    ASSERT_THAT((test + test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add4) {
  try {
    Entier test = Entier(0);
    Entier test2 = Entier(0);
    Entier test3 = Entier(0);
    ASSERT_THAT((test + test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add5) {
  try {
    Entier test = Entier(0);
    test += Entier(5);
    Entier test3 = Entier(5);
    ASSERT_THAT((test) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add6) {
  try {
    Entier test = Entier(100);
    test += Entier(5);
    Entier test3 = Entier(105);
    ASSERT_THAT((test) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add7) {
  try {
    Entier test = Entier(50);
    test += Entier(0);
    Entier test3 = Entier(50);
    ASSERT_THAT((test) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, add8) {
  try {
    Entier test = Entier(0);
    test += Entier(5);
    Entier test3 = Entier(50);
    ASSERT_THAT((test) == test3, Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract1) {
  try {
    Entier test = Entier(0);
    Entier test2 = Entier(0);
    Entier test3 = Entier(0);
    ASSERT_THAT((test - test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract2) {
  try {
    Entier test = Entier(80);
    Entier test2 = Entier(40);
    Entier test3 = Entier(40);
    ASSERT_THAT((test - test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract3) {
  try {
    Entier test = Entier(8000);
    Entier test2 = Entier(4000);
    Entier test3 = Entier(4000);
    ASSERT_THAT((test - test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract4) {
  try {
    Entier test = Entier(2000);
    Entier test2 = Entier(4000);
    Entier test3 = Entier(-2000);
    ASSERT_THAT((test - test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract5) {
  try {
    Entier test = Entier(2000);
    Entier test2 = Entier(2000);
    Entier test3 = Entier(0);
    ASSERT_THAT((test - test2) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract6) {
  try {
    Entier test = Entier(2000);
    Entier test3 = Entier(1999);
    ASSERT_THAT((test - Entier(1)) == test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract7) {
  try {
    Entier test = Entier(200004);
    Entier test3 = Entier(75599);
    ASSERT_THAT((test - test3) == 124405, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, substract8) {
  try {
    Entier test = Entier(2000);
    Entier test3 = Entier(4001);
    ASSERT_THAT((test - test3) == -2001, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, different) {
  try {
    Entier test = Entier(2000);
    Entier test3 = Entier(1999);
    ASSERT_THAT(test != test3, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, different2) {
  try {
    Entier test = Entier(1999);
    Entier test3 = Entier(1999);
    ASSERT_THAT(test != test3, Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, different3) {
  try {
    Entier test = Entier(-500);
    Entier test3 = Entier(-500);
    ASSERT_THAT(test != test3, Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, different4) {
  try {
    Entier test = Entier(-500);
    ASSERT_THAT(test != 500, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, different5) {
  try {
    Entier test = Entier(1) - 1;
    ASSERT_THAT(test != 0, Eq(false));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, div1) {
  try {
    Entier test = Entier(1) / Entier(1);
    ASSERT_THAT(test, Eq(1));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, div2) {
  try {
    Entier test = Entier(1) / Entier(2);
    ASSERT_THAT(test, Eq(0));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, div3) {
  try {
    Entier test = Entier(4) / Entier(2);
    ASSERT_THAT(test, Eq(2));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, div4) {
  try {
    Entier test = Entier(8) / Entier(-2);
    ASSERT_THAT(test, Eq(-4));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, div5) {
  try {
    Entier test = Entier(-10) / Entier(-5);
    ASSERT_THAT(test, Eq(2));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, assign1) {
  try {
    Entier test = 1;
    ASSERT_THAT(test == 1, Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, multiply1) {
  try {
    Entier test1 = 5;
    Entier test2 = 5;
    Entier rep = 25;
    ASSERT_THAT(rep == (test1 * test2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, multiply2) {
  try {
    Entier test1 = 5;
    Entier test2 = 1001;
    Entier rep = 5005;
    ASSERT_THAT(rep == (test1 * test2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, multiply3) {
  try {
    Entier test1 = -5;
    Entier test2 = 11001;
    Entier rep = -55005;
    ASSERT_THAT(rep == (test1 * test2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, multiply4) {
  try {
    Entier test1 = 5;
    Entier rep = 5005;
    ASSERT_THAT(rep == (test1 * 1001), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, multiply5) {
  try {
    Entier test1 = -5;
    Entier test2 = -1001;
    Entier rep = 5005;
    ASSERT_THAT(rep == (test1 * test2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, multiply6) {
  try {
    Entier test1 = 30000;
    Entier test2 = 25485;
    Entier rep = 764550000;
    ASSERT_THAT(rep == (test1 * test2), Eq(true));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, multiply7) {
  try {
    Entier test1 = 1000000;
    Entier test2 = 1000000;
    ASSERT_THAT((test1 * test2).toString(), Eq("1000000000000"));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, factorielle) {
  try {
    std::vector<Entier> facts;
    facts.push_back(Entier(1));
    // compute 10!
    for (int i = 2; i <= 10; i++) {
      facts.push_back(facts[i - 2] * Entier(i));
    }
    ASSERT_THAT(facts[9], Eq(3628800));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

TEST(Entier, stringRep1) {
  ASSERT_THAT(Entier(123456789).toString(), Eq("123456789"));
}

TEST(Entier, stringRep2) {
  ASSERT_THAT(Entier(-123456789).toString(), Eq("-123456789"));
}

TEST(Entier, stringRepAndFactorial) {
  try {
    std::vector<Entier> facts;
    facts.push_back(Entier(1));
    // compute 10!
    for (int i = 2; i <= 80; i++) {
      facts.push_back(facts[i - 2] * Entier(i));
    }
    // factorial of 11
    ASSERT_THAT(facts[10].toString(), Eq("39916800"));
    // factorial of 15
    ASSERT_THAT(facts[14].toString(), Eq("1307674368000"));
    // factorial of 25
    ASSERT_THAT(facts[24].toString(), Eq("15511210043330985984000000"));
    // factorial of 35
    ASSERT_THAT(facts[34].toString(),
                Eq("10333147966386144929666651337523200000000"));
    // factorial of 40
    ASSERT_THAT(facts[39].toString(),
                Eq("815915283247897734345611269596115894272000000000"));
    // factorial of 50
    ASSERT_THAT(
      facts[49].toString(),
      Eq("30414093201713378043612608166064768844377641568960512000000000000"));
    // factorial of 60
    ASSERT_THAT(facts[59].toString(),
                Eq("83209871127413901442763411832233643807541726063612459524492"
                   "77696409600000000000000"));
    // factorial of 80
    ASSERT_THAT(
      facts[79].toString(),
      Eq("715694570462638022948115337231865321655846573423657525771094450582270"
         "39255480148842668944867280814080000000000000000000"));
  } catch (const std::exception& e) {
    FAIL() << "Error at test:" << e.what() << std::endl;
  }
}

}  // namespace

}  // namespace nombres
