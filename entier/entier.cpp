#include "entier/entier.hpp"

#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace nombres {

Entier::Entier() {
  // default value for positive
  _positive = true;
}

Entier::Entier(int number) {
  // if the value is negative, switch it
  if (number < 0) {
    _positive = false;
    number = number * (-1);
  } else {
    _positive = true;
  }
  // while the value exists we add the succesive modulos
  while (number > 0) {
    _number.push_back(number % ENTIER_BASE);
    number = number / ENTIER_BASE;
  }
}

Entier Entier::operator+(Entier const& b) const {
  // we will call A *this and B the function parameter
  const Entier& A = (*this);
  const Entier& B = b;

  // if the operands are null, return a null number (avoid handling negative
  // null values)
  if (A == 0 && B == 0) {
    return Entier(0);
  }

  // if the sign are different
  if (A._positive != B._positive) {
    // compute the difference of the two absolute values of A and B
    Entier B_abs = Entier(B);
    Entier A_abs = Entier(A);
    A_abs._positive = true;
    B_abs._positive = true;
    Entier result = A_abs - B_abs;
    // if the result is negative
    if (result._positive == false) {
      // give it the opposite sign of A
      result._positive = !(A._positive);
    } else {
      // if the result is positive
      // give it the sign of A
      result._positive = A._positive;
    }
    // return the result
    return result;
  }

  // if we reach here signs are the same
  // create a new Entier for the result
  // give it the appropriate size
  Entier result = Entier(0);
  result._positive = A._positive;
  // get the size of A and B
  size_t sizeA = A._number.size();
  size_t sizeB = B._number.size();

  int buffer = 0;

  // until we reach the largest size
  for (size_t i = 0; i < sizeA || i < sizeB; i++) {
    // if possible, add A ith member
    if (i < sizeA) {
      buffer += A._number[i];
    }
    // if possible, add B ith member
    if (i < sizeB) {
      buffer += B._number[i];
    }
    // add what fits in the ith member in the new number
    result._number.push_back(buffer % ENTIER_BASE);

    // update the buffer for the i+1th member by dividing
    buffer /= ENTIER_BASE;
  }

  // if buffer is not empty at the end
  if (buffer != 0) {
    // push it
    result._number.push_back(buffer);
  }

  // detect negative numbers
  // troubleShootNegativeCoefs(result);

  // return result
  return result;
}

Entier Entier::operator+(int number) const {
  return Entier(*this) + Entier(number);
}

Entier& Entier::operator+=(Entier const& number) {
  Entier A = Entier(0);
  A = *this + number;
  *this = A;
  return *this;
}

Entier& Entier::operator+=(int number) {
  *this = *this + Entier(number);
  return *this;
}

Entier Entier::operator-(int other) const { return *this - Entier(other); }

Entier Entier::operator-(Entier const& b) const {
  // we will call A *this and B the function parameter
  const Entier& A = (*this);
  const Entier& B = b;

  // if B is null return A
  if (B == 0) {
    return Entier(A);
  }

  // if A is null return -B
  if (A == 0) {
    Entier result = A;
    result._positive = !result._positive;
    return result;
  }

  // if the sign are different
  if (A._positive != B._positive) {
    // compute the sum of the two absolute values of A and B
    Entier B_abs = Entier(B);
    Entier A_abs = Entier(A);
    A_abs._positive = true;
    B_abs._positive = true;
    Entier result = A_abs + B_abs;
    // give the sign of A
    result._positive = A._positive;
    // return the result
    return result;
  }

  // if we reach here signs are the same

  // bool to set if we flip the numbers (we substract the small one)
  bool flipped;
  // if A is less than B aknowledge the flipping for later
  if (A < B) {
    flipped = true;
  } else {
    flipped = false;
  }
  // create a new Entier for the result
  // give it the appropriate sign
  Entier result = Entier(0);
  if (flipped) {
    result._positive = !A._positive;
  } else {
    result._positive = A._positive;
  }
  // get the size of A and B
  size_t sizeA = A._number.size();
  size_t sizeB = B._number.size();

  // buffer for the iterative difference
  int64_t buffer = 0;

  // until we reach the largest size
  for (size_t i = 0; i < sizeA || i < sizeB; i++) {
    // if possible, add A ith member (or substract if we flipped A and B)
    if (i < sizeA) {
      if (flipped) {
        buffer -= (int64_t)A._number[i];
      } else {
        buffer += (int64_t)A._number[i];
      }
    }
    // if possible, substract B ith member (or add if we flipped A and B)
    if (i < sizeB) {
      if (flipped) {
        buffer += (int64_t)B._number[i];
      } else {
        buffer -= (int64_t)B._number[i];
      }
    }

    // if the buffer is negative, take one unit from the upper number by
    // adding ENTIER_BASE to the number and setting buffer to -1
    if (buffer < 0) {
      result._number.push_back(ENTIER_BASE + (int)buffer);
      buffer = -1;
      // if not, then everything is alright and we can push the result as it is
    } else {
      // no need to modulo as the number can't be bigger than the base
      result._number.push_back((int)buffer);
      // reset the buffer as there's nothing to add or substract to the upper
      // level
      buffer = 0;
    }
  }

  // remove trailing zero
  if (result._number.size() > 1) {
    do {
      std::vector<int>::iterator it1 = result._number.end() - 1;
      if (*it1 == 0)
        result._number.pop_back();
      else
        break;
    } while (result._number.size() >= 1);
  }

  // if the number is a plain null item remove it
  if (result._number.size() == 1 && result._number[0] == 0) {
    result._number.pop_back();
  }

  // if the size is zero, reset the sign (we say 0 are always positive)
  if (result._number.size() == 0) {
    result._positive = true;
  }

  // detect negative numbers
  // troubleShootNegativeCoefs(result);

  return result;
}

Entier& Entier::operator-=(Entier const& number) {
  *this = *this - number;
  return *this;
}

Entier& Entier::operator-=(int number) {
  *this = *this - Entier(number);
  return *this;
}

bool Entier::operator==(const Entier& other) const {
  // return false if the signs are different
  if (_positive != other._positive) {
    return false;
  }

  // if the representation size if different then the number are
  if (_number.size() != other._number.size()) {
    return false;
  }

  // for each element in representation compare it
  for (size_t i(_number.size()); i > 0; --i) {
    if (_number[i - 1] != other._number[i - 1]) return false;
  }

  // if we reached here, numbers are equal
  return true;
}

bool Entier::operator==(int number) const {
  // handle zero and skip a step
  if (number == 0 && _number.size() == 0) return true;
  // if not zero, resolve to conversion to Entier
  return Entier(number) == *this;
}

bool Entier::operator!=(const Entier& number) const {
  return !(*this == number);
};

bool Entier::operator!=(int number) const { return !(*this == Entier(number)); }

Entier Entier::operator*(Entier const& b) const {
  // we will call A *this and B the function parameter
  const Entier& A = (*this);
  const Entier& B = b;

  // create the number to store the result
  Entier result = 0;

  // deduct the sign
  // if differents, negative
  if (A._positive != B._positive) {
    result._positive = false;
    // if same, positive
  } else {
    result._positive = true;
  }

  int128_t buffer;

  // for each ith element of A
  for (size_t i = 0; i < A._number.size(); i++) {
    // for each jth element of B
    for (size_t j = 0; j < B._number.size(); j++) {
      // multiply ith A and jth B together
      buffer = (int128_t)A._number[i] * (int128_t)B._number[j];
      // if the i+jth element does not exists push zeros through it and one
      // beyond
      while ((i + j + 1) >= result._number.size()) {
        result._number.push_back(0);
      }
      // add the modulo base result to the (i+jth element)
      result._number[i + j] += (int)(buffer % (int128_t)ENTIER_BASE);
      // push the base divided result to the next element of result
      result._number[i + j + 1] +=
        (int)((int128_t)buffer / (int128_t)ENTIER_BASE);
    }
  }

  // remove trailing zeros
  if (result._number.size() > 1) {
    do {
      std::vector<int>::iterator it1 = result._number.end() - 1;
      if (*it1 == 0)
        result._number.pop_back();
      else
        break;
    } while (result._number.size() >= 1);
  }

  // if the number is a plain null item remove it
  if (result._number.size() == 1 && result._number[0] == 0) {
    result._number.pop_back();
  }

  // detect negative numbers
  // troubleShootNegativeCoefs(result);

  // we're done
  return result;
}

Entier Entier::operator*(const int number) const {
  return (*this) * Entier(number);
}

Entier Entier::operator=(const int& num) {
  int number = num;
  // clear the array
  _number.clear();
  // do the same iteration through modulo as in the constructor
  // if the value is negative, switch it
  if (number < 0) {
    _positive = false;
    number = number * (-1);
  } else {
    _positive = true;
  }
  // while the value exists we add the succesive modulos
  while (number > 0) {
    _number.push_back(number % ENTIER_BASE);
    number = number / ENTIER_BASE;
  }

  return *this;
}

bool Entier::operator<(const Entier b) const {
  // we will call A *this and B the function parameter
  const Entier& A = (*this);
  const Entier& B = b;

  if (A <= B && A != B)
    return true;
  else
    return false;
}

bool Entier::operator<=(const Entier b) const {
  // we will call A *this and B the function parameter
  const Entier& A = (*this);
  const Entier& B = b;

  // bool to know if the sign require flipping the result
  bool flipped = true;

  // if the signs are different
  if (A._positive != B._positive) {
    // if A is the negative one, return true
    if (A._positive == false)
      return true;
    else
      return false;
  } else {
    // if the signs are both negative (eg A is negative)
    // we need to know that the order will be flipped
    // else, we need to know the result is the canonical one
    flipped = !A._positive;
  }


  
  // if the size are different
  if (A._number.size() != B._number.size()) {
    // return true if A's size is smaller, false if not
    if (!flipped)
      return (A._number.size() < B._number.size());
    else
      return (B._number.size() < A._number.size());
  }


  // if we reach here then the sizes are equal

  // for each item in the _number array biggest first
  for (int i = 0; i < ((int)A._number.size()); i++) {
    // if A and B are both positive
    if (!flipped) {
      // if A ith member is smaller than Bs
      if (A._number[i] < B._number[i]) {
        // A is smaller
        return true;
        // if B ith member is bigger than Bs
      } else if (A._number[i] > B._number[i]) {
        // A is bigger
        return false;
      }
      // if they are both negative
    } else {
      
      // if B ith member is smaller than As
      if (B._number[i] < A._number[i]) {
        // A is smaller
        return true;
        // if B ith member is bigger than As
      } else if (A._number[i] < B._number[i]) {
        // B is bigger
        return false;
      }
    }
    // if none of these is true (equal) then go to next iter
  }

  // if all members are equal then the two number are equal
  // and we can return
  return true;
}

// we use the opposite operator to implement this one
bool Entier::operator>=(const Entier b) const { return (b <= (*this)); }
bool Entier::operator>(const Entier b) const { return (b < (*this)); }

std::string Entier::toString() const {
  std::ostringstream stream;
  stream << *this;
  return stream.str();
}

// return the string representation, requires ENTIER_BASE to be a power of 10
std::ostream& operator<<(std::ostream& leftStream, Entier const& nb) {
  // if the represation array is empty or filled with zeros return 0
  if (nb._number.size() == 0) return leftStream << '0';
  bool isEmpty = true;
  for (size_t i = 0; i < nb._number.size(); i++) {
    if (nb._number[i] != 0) isEmpty = false;
  }
  if (isEmpty) {
    return leftStream << '0';
  }

  // if the number is negative output the sign
  if (nb._positive == false) leftStream << '-';

  // get the padding at each iteration
  int padding = log10(ENTIER_BASE);

  // the current number of zeros we need to print
  int nbZeros;

  // for each number in reversed order
  for (size_t i = nb._number.size() - 1; (int)i >= 0; i--) {
    // if the number is the last
    if (i == nb._number.size() - 1) {
      // padding will be 0 (no need to put 0 for the last one)
      nbZeros = 0;
    } else {
      // if not
      // get the number of 0 left to print
      nbZeros = padding - std::__cxx11::to_string(nb._number[i]).length();
    }

    // print the required 0 padding
    while (nbZeros > 0) {
      leftStream << '0';
      nbZeros--;
    }

    // print the number
    leftStream << std::__cxx11::to_string(nb._number[i]);
  }

  return leftStream;
}

Entier Entier::operator%(const int number) const {
  // necesary variables
  Entier result = 0;
  int degree;
  int buffer;
  int base_mod = ENTIER_BASE % number;
  // for each number member
  for (size_t i = 0; i < _number.size(); i++) {
    // get the degree modulo
    degree = ((int)i) % number;
    result +=
      ((_number[i] % number) * ((int)round(pow(base_mod, degree)))) % number;
    // cast result back to int for additional modulo
    buffer = std::stoi(result.toString());
    buffer = buffer % number;
    // restore the number once modulo has been performed
    result = buffer;
  }

  return result;
}

Entier::operator float() const { return std::stof(toString()); }

/*
Entier::operator uint128_t() const {
  // create an entier to keep track of potential overflow
  Entier monitor = Entier(0);
  // create an uint128_t to return
  uint128_t response;
}
*/

Entier Entier::operator/(Entier const& number) const {
  /* Naive euclidian division algorithm */

  // throw an error if divison per zero
  if (number == 0) {
    throw std::runtime_error("Entier::operator/: null division error");
  }

  // copy required to access the size member of array
  Entier copy = Entier(*this);

  // if we are trying to divide with the base, shift the number array instead
  if (number == ENTIER_BASE && copy._number.size() >= (size_t)1) {
    Entier rep = Entier(*this);
    rep._number.erase(rep._number.begin());
    return rep;
  } else if (number == ENTIER_BASE) {
    return Entier(0);
  }

  // create a number to hold the sum
  Entier sum = Entier(0);
  // create a number to hold the result (set it to -1)
  Entier res = Entier(0);
  // create an absolute value copy of *(this) and denominator
  // name number A and B (A/B)
  Entier A = Entier(*this);
  A._positive = true;
  Entier B = Entier(number);
  B._positive = true;

  // while the sum is smaller or equal than A
  while (sum <= A) {
    // add B to the sum
    sum = sum + B;
    // increment result
    if (sum <= A) res = res + 1;
  }

  // if A and B were of the same sign
  if ((*this)._positive == number._positive) {
    // return a positive result
    return res;
  } else {
    // else
    // return a negative result
    res._positive = false;
    return res;
  }
}

Entier Entier::operator/(const int number) const {
  Entier res = Entier(0);
  res = res / Entier(number);
  return res;
}

void Entier::troubleShootNegativeCoefs(Entier a) const {
  for(size_t i=0;i<a._number.size();i++) {
    if(a._number[i]<0) {
      int * number = 0;
      (*number) = 4;
      throw std::runtime_error("A negative number was detected in coefficient of polynomial storing numbers");
    }
  }
}

bool Entier::operator<(const int b) const { return (*this) < Entier(b); }
bool Entier::operator>(const int b) const { return (*this) > Entier(b); }
bool Entier::operator>=(const int b) const { return (*this) >= Entier(b); }
bool Entier::operator<=(const int b) const { return (*this) <= Entier(b); }

}  // namespace nombres