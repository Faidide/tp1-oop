#ifndef DEF_ENTIER_HPP
#define DEF_ENTIER_HPP

#include <iostream>
#include <string>
#include <vector>

// WARNINGS:
// - STRING CONVERSION REQUIRES THE BASE TO BE A POWER OF 10 TO HAVE
#define ENTIER_BASE 100000000

typedef __int128 int128_t;
typedef unsigned __int128 uint128_t;


// use the new suites namespace
namespace nombres {

// define a class
class Entier {
 public:
  // constructors
  Entier();
  Entier(int number);
  // function to get the string representation
  std::string toString() const;
  // arithmetic operators
  Entier operator+(Entier const& number) const;
  Entier& operator+=(Entier const& number);
  Entier operator+(int number) const;
  Entier& operator+=(int number);
  Entier operator-(Entier const& number) const;
  Entier operator-(int other) const;
  Entier& operator-=(Entier const& number);
  Entier& operator-=(int number);
  Entier operator*(Entier const& number) const;
  Entier operator*(const int number) const;
  Entier operator/(Entier const& number) const;
  Entier operator/(const int number) const;
  // modulo operator
  Entier operator%(const int number) const;
  // comparison operators
  bool operator==(const Entier& number) const;
  bool operator==(int number) const;
  bool operator!=(const Entier& number) const;
  bool operator!=(int number) const;
  Entier operator=(const int& number);
  bool operator<(const Entier b) const;
  bool operator>(const Entier b) const;
  bool operator>=(const Entier b) const;
  bool operator<=(const Entier b) const;
  // integer comparison
  bool operator<(const int b) const;
  bool operator>(const int b) const;
  bool operator>=(const int b) const;
  bool operator<=(const int b) const;
  // allow casting to float and uint64_t
  operator float() const;
  // operator uint64_t() const;
  // without this one, we can't print it as we print ints in the Rationnel class
  friend std::ostream& operator<<(std::ostream& leftStream,
                                  Entier const& number);

 private:
  std::vector<int> _number;
  bool _positive;
  // used for debugging
  void troubleShootNegativeCoefs (Entier a) const;
};

}  // namespace nombres

#endif  // DEF_ENTIER_HPP
