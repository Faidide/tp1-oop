# TP1
## Install bazel

Bazel is an Open-Source Build and Test Tool similar to Make, Maven, and Gradle. It uses a Human-Readable, High-Level build Language. Bazel supports projects in Multiple Languages and Builds outputs for Multiple Platforms.


Bazel is available in many repositories, including as for debian and its derivatives from an apt repository. The official [installation page](https://docs.bazel.build/versions/master/install-ubuntu.html) is titled ubuntu only because Google doesn't run on other distro in its Continuous Integration Tests, but they are indeed supposed to work on debian and different ubuntu versions. There is also a binary installer.


## Bazel usage

*Important note*: Bazel is just like npm or pip with virtualenv, at first launch, it will take a lot of time to download and install the required dependencies.

Build with
```
bazel build //...
```

Test with
```
bazel test //...
```

Run tests with the entire test output
```
bazel test --test_output=all //... 
```

Run with
```
bazel run //main:main
```

Run GNU Debugger (change target accordingly):
```
bazel build //... --compilation_mode=dbg -s
gdb bazel-bin/entier/unit_test
```


## Folder Organization
The main executable is built from the main folder. All other libraries have their own folder, and inside each library folder, there is a test folder.

Sometimes, I'm also adding a "doc" and a "res" folder.

## Clang formatting
```bash
clang-format */*.cpp */*.hpp -i
```

## Continuous Integration Pipelines at GitLab
I use the CI pipelines in `.gitlab-ci.yml`

## Semantic Versionning
The last of the pipelines is a semantic versionning tool that will automatically generate version numbers and tags according to the commit messages that follow the [angular commit message format](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines).


Accepted commit prefixes are: `feat`, `fix`, `docs`, `style`, `refactor`, `perf`, `test`, `chore`, `BREAKING CHANGE`. 