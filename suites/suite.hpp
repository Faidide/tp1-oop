#ifndef DEF_SUITE_HPP
#define DEF_SUITE_HPP

#include <functional>
#include <vector>

#include "rationnel/rationnel.hpp"

// use the new suites namespace
namespace suites {

// template to handle many types
template <typename T>
// define a class for the suites
class Suite {
 public:
  // constructor using intial terms and function to generate next term
  Suite(std::vector<T> initialTerms,
        std::function<T(std::vector<T>)> iteratorFunction);
  // return the rank-th term of the sequence (compute it)
  T computeUntill(int rank);

 private:
  // the number of term required to compute the next term
  int _recursionOrder;
  // the current term (index of last written starting sequence at zero)
  int _currentIndex;
  // vectors of initial and current terms
  std::vector<T> _initialTerms;
  std::vector<T> _currentTerms;
  // the function to compute the next term
  std::function<T(std::vector<T>)> _iteratorFunction;
};

}  // namespace suites

#endif  // DEF_SUITE_HPP
