#include "suites/suite.hpp"
#include "entier/entier.hpp"
// include Google Mock and iostream
#include <gmock/gmock.h>

#include <cmath>
#include <iostream>
#include <stdexcept>

// iterative function for a geometric sequence
nombres::Rationnel<int> suite1(std::vector<nombres::Rationnel<int>> terms) {
  nombres::Rationnel<int> two(2, 1);
  return terms[0] * two;
}

// iterative function for a geometric sequence
nombres::Rationnel<int> suite2(std::vector<nombres::Rationnel<int>> terms) {
  return terms[0] + terms[1];
}

// iterative function for a geometric sequence
int suite3(std::vector<int> terms) { return terms[0] + terms[1]; }

// Un iteration function
double unIteration(std::vector<double> terms) {
  return 2003 - (6002 / terms[1]) + (4000 / (terms[1] * terms[0]));
}
// Un iteration function with Rationnel
nombres::Rationnel<int> unIterationRationnel(
  std::vector<nombres::Rationnel<int>> terms) {
  return nombres::Rationnel<int>(2003, 1) -
         (nombres::Rationnel<int>(6002, 1) / terms[1]) +
         (nombres::Rationnel<int>(4000, 1) / (terms[1] * terms[0]));
}
nombres::Rationnel<nombres::Entier> unIterationRationnelEntier(
  std::vector<nombres::Rationnel<nombres::Entier>> terms) {
  nombres::Rationnel<nombres::Entier> m1 = nombres::Rationnel<nombres::Entier>(terms[1].getNumer(), terms[1].getDenom());
  m1 = m1 * terms[0];
  nombres::Rationnel<nombres::Entier> A = nombres::Rationnel<nombres::Entier>(nombres::Entier(2003), nombres::Entier(1));
  nombres::Rationnel<nombres::Entier> B = nombres::Rationnel<nombres::Entier>(nombres::Entier(6002), nombres::Entier(1)) / terms[1];
  nombres::Rationnel<nombres::Entier> C = nombres::Rationnel<nombres::Entier>(nombres::Entier(4000), nombres::Entier(1)) / m1;
  nombres::Rationnel<nombres::Entier> Res = A - B + C;
  return Res;
}
// Un iteration function but with floats
float unIterationFloat(std::vector<float> terms) {
  return 2003 - (6002 / terms[1]) + (4000 / (terms[1] * terms[0]));
}

// Vn iteration function
double a = 2;
nombres::Rationnel<int> aRationnel = nombres::Rationnel<int>(2, 1);
double vnIteration(std::vector<double> terms) {
  return (terms[0] / 2.0) + (a / (2.0 * terms[0]));
}
// Vn iteration function with Rationnel class
nombres::Rationnel<int> vnIterationRationnel(
  std::vector<nombres::Rationnel<int>> terms) {
  return (terms[0] / nombres::Rationnel<int>(2, 1)) +
         (aRationnel / (nombres::Rationnel<int>(2, 1) * terms[0]));
}

float vnIterationFloat(std::vector<float> terms) {
  return (terms[0] / 2.0) + ((float)a / (2.0 * terms[0]));
}

namespace suites {
namespace {

using testing::Eq;

// testing the object creation
TEST(Suite, creation) {
  // the initial terms of the sequence
  std::vector<nombres::Rationnel<int>> initialTerms;
  // add the only required initial term
  initialTerms.push_back(nombres::Rationnel<int>(1, 1));
  // create the suite object
  Suite<nombres::Rationnel<int>> maSuite(initialTerms, suite1);
}

// testing the computation
TEST(Suite, firstOrderGeometric) {
  // the initial terms of the sequence
  std::vector<nombres::Rationnel<int>> initialTerms;
  // add the only required initial term
  initialTerms.push_back(nombres::Rationnel<int>(1, 1));
  // create the suite object
  Suite<nombres::Rationnel<int>> maSuite(initialTerms, suite1);
  try {
    // get the 4th term
    nombres::Rationnel<int> resultat = maSuite.computeUntill(4);
    // test the reslt
    ASSERT_THAT(resultat.valeurApprox(), Eq(8.0));
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in firstOrderGeometric:" << e.what() << std::endl;
  }
}

// testing the successive computation
TEST(Suite, firstOrderGeometricRetry) {
  // the initial terms of the sequence
  std::vector<nombres::Rationnel<int>> initialTerms;
  // add the only required initial term
  initialTerms.push_back(nombres::Rationnel<int>(1, 1));
  // create the suite object
  Suite<nombres::Rationnel<int>> maSuite(initialTerms, suite1);
  // get the 4th term
  nombres::Rationnel<int> resultat = maSuite.computeUntill(7);
  // retry
  try {
    // get the 4th term
    nombres::Rationnel<int> resultat = maSuite.computeUntill(4);
    // test the reslt
    ASSERT_THAT(resultat.valeurApprox(), Eq(8.0));
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in firstOrderGeometricRetry:" << e.what() << std::endl;
  }
}

// testing computing second order fibonacci
TEST(Suite, fibonacci) {
  // the initial terms of the sequence
  std::vector<nombres::Rationnel<int>> initialTerms;
  // add the two inital terms of fibonacci sequence
  initialTerms.push_back(nombres::Rationnel<int>(0, 1));
  initialTerms.push_back(nombres::Rationnel<int>(1, 1));
  // create the suite object
  Suite<nombres::Rationnel<int>> maSuite(initialTerms, suite2);
  try {
    // get the 4th term
    nombres::Rationnel<int> resultat = maSuite.computeUntill(7);
    // test the reslt
    ASSERT_THAT(resultat.valeurApprox(), Eq(8.0));
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in fibonacci:" << e.what() << std::endl;
  }
}

// testing computing second order fibonacci
TEST(Suite, fibonacciIntegers) {
  // the initial terms of the sequence
  std::vector<int> initialTerms;
  // add the two inital terms of fibonacci sequence
  initialTerms.push_back(0);
  initialTerms.push_back(1);
  // create the suite object
  Suite<int> maSuite(initialTerms, suite3);
  try {
    // get the 4th term
    int resultat = maSuite.computeUntill(7);
    // test the reslt
    ASSERT_THAT(resultat, Eq(8));
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in fibonacciIntegers:" << e.what() << std::endl;
  }
}


TEST(Suite, VnDoubleSequence) {
  // the initial terms of the sequence
  std::vector<double> initialTerms;
  // add the two inital terms of fibonacci sequence
  initialTerms.push_back(1.0);
  // create the suite object
  Suite<double> maSuite(initialTerms, vnIteration);
  try {
    // get the 4th term
    double resultat = maSuite.computeUntill(7);
    // test the result is close to what is expected
    ASSERT_THAT(abs(resultat - 1.41421) < 0.00001, Eq(true));
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in UnDoubleSequence:" << e.what() << std::endl;
  }
}


TEST(Suite, UnDoubleSequence) {
  // the initial terms of the sequence
  std::vector<double> initialTerms;
  // add the two inital terms of fibonacci sequence
  initialTerms.push_back(3.0 / 2.0);
  initialTerms.push_back(5.0 / 3.0);
  // create the suite object
  Suite<double> maSuite(initialTerms, unIteration);
  try {
    double resultat[20];
    // compute all terms
    for(int i=2;i<20;i++) {
      resultat[i] = maSuite.computeUntill(i);
    }
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in UnDoubleSequence:" << e.what() << std::endl;
  }
}


TEST(Suite, UnRationnelSequence) {
  // the initial terms of the sequence
  std::vector<nombres::Rationnel<int>> initialTerms;
  // add the two inital terms of fibonacci sequence
  initialTerms.push_back(nombres::Rationnel<int>(3.0, 2.0));
  initialTerms.push_back(nombres::Rationnel<int>(5.0, 3.0));
  // create the suite object
  Suite<nombres::Rationnel<int>> maSuite(initialTerms, unIterationRationnel);
  try {
    std::vector<nombres::Rationnel<int>> resultat;
    // compute all terms
    for(int i=2;i<20;i++) {
      resultat.push_back(maSuite.computeUntill(i));
    }
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in UnRationnelSequence:" << e.what() << std::endl;
  }
}

TEST(Suite, UnFloatSequence) {
  // the initial terms of the sequence
  std::vector<float> initialTerms;
  // add the two inital terms of fibonacci sequence
  initialTerms.push_back(3.0f / 2.0f);
  initialTerms.push_back(5.0f / 3.0f);
  // create the suite object
  Suite<float> maSuite(initialTerms, unIterationFloat);
  try {
    // get the 7th term
    float resultat = maSuite.computeUntill(7);
    SUCCEED();
  } catch (const std::exception& e) {
    // make the test fail with the error message
    FAIL() << "Error in UnFloatSequence:" << e.what() << std::endl;
  }
}

}  // namespace

}  // namespace suites
