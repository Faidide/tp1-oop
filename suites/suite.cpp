#include "suites/suite.hpp"
#include "entier/entier.hpp"

#include <iostream>
#include <stdexcept>

namespace suites {

// constructor using intial terms and function to generate next term
template <typename T>
Suite<T>::Suite(std::vector<T> initialTerms,
                std::function<T(std::vector<T>)> iteratorFunction) {
  // save the function to compute iterations
  _iteratorFunction = iteratorFunction;
  // save the recursionOrder
  _recursionOrder = initialTerms.size();
  // initialize the initial terms of the sequence
  _initialTerms = initialTerms;
  // set the currentIndex
  _currentIndex = _recursionOrder;
  // set the current terms of the sequence (the initial ones)
  _currentTerms = initialTerms;
  return;
}

// return the rank-th term of the sequence (compute it)
template <typename T>
T Suite<T>::computeUntill(int rank) {
  // won't compute below a rank that is too small
  if (rank < 0) {
    throw std::runtime_error("Suite::computeUntill: rank is too small");
  }

  // return the initial terms if asked for something not required to compute
  if(rank < _recursionOrder) {
    return _initialTerms[rank];
  }

  // initialize the first terms
  _currentTerms = _initialTerms;
  // set the currentIndex
  _currentIndex = _recursionOrder;

  // main loop
  while (_currentIndex != rank) {
    // push the new term to the end of the currentTerms index
    _currentTerms.push_back(_iteratorFunction(_currentTerms));
    // shift the vector array
    _currentTerms.erase(_currentTerms.begin());
    // increment the index
    _currentIndex++;
  }

  // return the last term
  return _currentTerms[_currentTerms.size() - 1];
}

// this helps avoiding linking errors
template class Suite<nombres::Rationnel<int>>;
template class Suite<nombres::Rationnel<nombres::Entier>>;
template class Suite<int>;
template class Suite<double>;
template class Suite<float>;

}  // namespace suites