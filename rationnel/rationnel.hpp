#ifndef DEF_RATIONNEL_HPP
#define DEF_RATIONNEL_HPP

#define MAX_INT 2147483646

// use the new nombres namespace
namespace nombres {

// template to handle many types
template <typename T>
// define the class holding the rational numbers
class Rationnel {
 public:
  // constructor
  Rationnel(T num, T den);
  // destructor
  ~Rationnel();
  // display the rational in a rational manner
  void afficher();
  // return the float division between denominator and numerator
  double valeurApprox();
  // transform the rational to the smallest num/den that are equal
  void simplifie();
  // add an integer
  void ajouter(int n);
  // add with another rational
  void ajouter(const Rationnel<T>& r);
  // substract with an integer
  void soustraire(int n);
  // substract with another rational
  void soustraire(const Rationnel<T>& r);
  // multiply with an integer
  void multiplier(int n);
  // multiply with another rational
  void multiplier(const Rationnel<T>& r);
  // divide with an integer
  void diviser(int n);
  // divide with another rational
  void diviser(const Rationnel<T>& r);
  // operator of equality between two rationals
  bool operator==(const Rationnel<T>& left);
  // accessors for numerator and denom
  T getDenom() const;
  T getNumer() const;
  // usual operators
  Rationnel<T> operator+(const Rationnel<T>& b);
  Rationnel<T> operator*(const Rationnel<T>& b);
  Rationnel<T> operator/(const Rationnel<T>& b);
  Rationnel<T> operator-(const Rationnel<T>& b);

 private:
  T _numerateur;
  T _denominateur;
  // euclide algorithm
  T _gcd(T a, T b);
};

}  // namespace nombres

#endif  // DEF_RATIONNEL_HPP
