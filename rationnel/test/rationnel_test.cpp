#include "rationnel/rationnel.hpp"

#include "entier/entier.hpp"
// include Google Mock and iostream
#include <gmock/gmock.h>

#include <iostream>

namespace nombres {
namespace {

using testing::Eq;

// testing the object creation
TEST(Rationnel, creation) {
  Rationnel<int> monRationnel(1, 1);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(1.0));
}

// testing the object creation
TEST(Rationnel, creationEntier) {
  Rationnel<Entier> monRationnel(Entier(1), Entier(1));
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(1.0));
}

// testing the null division failure by catching it
TEST(Rationnel, creationFailure) {
  try {
    Rationnel<int> monRationnel(1, 0);
    FAIL();
  } catch (const std::exception& e) {
    SUCCEED();
  }
}

// testing the null division failure by catching it
TEST(Rationnel, creationFailureEntier) {
  try {
    Rationnel<Entier> monRationnel(Entier(1), Entier(0));
    FAIL();
  } catch (const std::exception& e) {
    SUCCEED();
  }
}

// testing the approximative value
TEST(Rationnel, valeurApprox1) {
  Rationnel<int> monRationnel(1, 2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.5));
}

// testing the approximative value
TEST(Rationnel, valeurApprox1Entier) {
  Rationnel<Entier> monRationnel(Entier(1), Entier(2));
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.5));
}

// testing the approximative value
TEST(Rationnel, valeurApprox2) {
  Rationnel<int> monRationnel(1, 4);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.25));
}

// testing the approximative value
TEST(Rationnel, valeurApprox3) {
  Rationnel<int> monRationnel(-1, 4);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(-0.25));
}

// testing the approximative value
TEST(Rationnel, valeurApprox3Entier) {
  Rationnel<Entier> monRationnel(Entier(-1), Entier(4));
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(-0.25));
}

// testing the approximative value
TEST(Rationnel, valeurApprox4) {
  Rationnel<int> monRationnel(-1, -4);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.25));
}

// testing the approximative value
TEST(Rationnel, valeurApprox4Entier) {
  Rationnel<Entier> monRationnel(Entier(-1), Entier(-4));
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.25));
}

// testing the approximative value
TEST(Rationnel, valeurApprox5) {
  Rationnel<int> monRationnel(1, -4);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(-0.25));
}

// testing the fraction simplification
TEST(Rationnel, simplifier) {
  Rationnel<int> monRationnel(2, 8);
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.getNumer(), Eq(1));
  ASSERT_THAT(monRationnel.getDenom(), Eq(4));
}

// testing the fraction simplification
TEST(Rationnel, simplifierEntier) {
  Rationnel<Entier> monRationnel(Entier(2), Entier(8));
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.getNumer(), Eq(1));
  ASSERT_THAT(monRationnel.getDenom(), Eq(4));
}

// testing the fraction simplification
TEST(Rationnel, simplifier2) {
  Rationnel<int> monRationnel(2000, 8000);
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.getNumer(), Eq(1));
  ASSERT_THAT(monRationnel.getDenom(), Eq(4));
}

// testing the fraction simplification
TEST(Rationnel, simplifier2Entier) {
  Rationnel<Entier> monRationnel(Entier(2000), Entier(8000));
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.getNumer(), Eq(1));
  ASSERT_THAT(monRationnel.getDenom(), Eq(4));
}

// testing the fraction simplification
TEST(Rationnel, simplifier3) {
  Rationnel<int> monRationnel(-2000, -8000);
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.getNumer(), Eq(1));
  ASSERT_THAT(monRationnel.getDenom(), Eq(4));
}

// testing the fraction simplification
TEST(Rationnel, simplifier3Entier) {
  Rationnel<Entier> monRationnel(Entier(-2000), Entier(-8000));
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.getNumer(), Eq(1));
  ASSERT_THAT(monRationnel.getDenom(), Eq(4));
}

// testing the fraction simplification
TEST(Rationnel, simplifier4) {
  Rationnel<int> monRationnel(2000, -8000);
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(-0.25));
}

// testing the fraction simplification
TEST(Rationnel, simplifier5) {
  Rationnel<int> monRationnel(-2000, 8000);
  // call to simplify the fraction
  monRationnel.simplifie();
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(-0.25));
}

// testing the addition function ajouter with an integer
TEST(Rationnel, ajouter1) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(5, 4);
  monRationnel.ajouter(1);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(monRationnel2.valeurApprox()));
}

// testing the addition function ajouter with an integer
TEST(Rationnel, ajouter1Entier) {
  Rationnel<Entier> monRationnel(Entier(1), Entier(4));
  Rationnel<Entier> monRationnel2(Entier(5), Entier(4));
  monRationnel.ajouter(1);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(monRationnel2.valeurApprox()));
}

// testing the addition function ajouter with a rational
TEST(Rationnel, ajouter2) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(2, 4);
  monRationnel.ajouter(monRationnel2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.75));
}

// testing the addition function ajouter with a rational
TEST(Rationnel, ajouter2Entier) {
  Rationnel<Entier> monRationnel(Entier(1), Entier(4));
  Rationnel<Entier> monRationnel2(Entier(2), Entier(4));
  monRationnel.ajouter(monRationnel2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.75));
}

// testing the addition function ajouter with a rational
TEST(Rationnel, ajouter3) {
  Rationnel<int> monRationnel(-1, 4);
  Rationnel<int> monRationnel2(2, 4);
  monRationnel.ajouter(monRationnel2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.25));
}

// testing the addition function ajouter with a rational
TEST(Rationnel, ajouter4) {
  Rationnel<int> monRationnel(-1, 4);
  Rationnel<int> monRationnel2(-2, -4);
  monRationnel.ajouter(monRationnel2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(0.25));
}

// testing the addition function ajouter with a rational
TEST(Rationnel, ajouter5) {
  Rationnel<int> monRationnel(-1, 4);
  Rationnel<int> monRationnel2(-2, 4);
  monRationnel.ajouter(monRationnel2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(-0.75));
}

// testing the substract function soustraire with an integer
TEST(Rationnel, soustraire1) {
  Rationnel<int> monRationnel(5, 4);
  Rationnel<int> monRationnel2(1, 4);
  monRationnel.soustraire(1);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(monRationnel2.valeurApprox()));
}

// testing the substract function soustraire with another rational
TEST(Rationnel, soustraire2) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(2, 4);
  monRationnel2.soustraire(monRationnel);
  ASSERT_THAT(monRationnel2.valeurApprox(), Eq(0.25));
}

// testing the substract function soustraire with another rational
TEST(Rationnel, soustraire2Entier) {
  Rationnel<Entier> monRationnel(Entier(1), Entier(4));
  Rationnel<Entier> monRationnel2(Entier(2), Entier(4));
  monRationnel2.soustraire(monRationnel);
  ASSERT_THAT(monRationnel2.valeurApprox(), Eq(0.25));
}

// testing the substract function soustraire with another rational
TEST(Rationnel, soustraire3) {
  Rationnel<int> monRationnel(-1, 4);
  Rationnel<int> monRationnel2(2, 4);
  monRationnel2.soustraire(monRationnel);
  ASSERT_THAT(monRationnel2.valeurApprox(), Eq(0.75));
}

// testing the substract function soustraire with another rational
TEST(Rationnel, soustraire4) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(-2, 4);
  monRationnel2.soustraire(monRationnel);
  ASSERT_THAT(monRationnel2.valeurApprox(), Eq(-0.75));
}

// testing the multiply function multiplier with an integer
TEST(Rationnel, multiplier1) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(2, 4);
  monRationnel.multiplier(2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(monRationnel2.valeurApprox()));
}

// testing the multiply function multiplier with an integer
TEST(Rationnel, multiplier1Entier) {
  Rationnel<Entier> monRationnel(Entier(1), Entier(4));
  Rationnel<Entier> monRationnel2(Entier(2), Entier(4));
  monRationnel.multiplier(2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(monRationnel2.valeurApprox()));
}

// testing the multiply function multiplier with another rational
TEST(Rationnel, multiplier2) {
  Rationnel<int> monRationnel(1, 2);
  Rationnel<int> monRationnel2(1, 2);
  monRationnel2.multiplier(monRationnel);
  ASSERT_THAT(monRationnel2.valeurApprox(), Eq(0.25));
}

// testing the divide function diviser with an integer
TEST(Rationnel, diviser1) {
  Rationnel<int> monRationnel(2, 4);
  Rationnel<int> monRationnel2(1, 4);
  monRationnel.diviser(2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(monRationnel2.valeurApprox()));
}

// testing the divide function diviser with an integer
TEST(Rationnel, diviser1Entier) {
  Rationnel<Entier> monRationnel(Entier(2), Entier(4));
  Rationnel<Entier> monRationnel2(Entier(1), Entier(4));
  monRationnel.diviser(2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(monRationnel2.valeurApprox()));
}

// testing the divide function diviser with another rational
TEST(Rationnel, diviser2) {
  Rationnel<int> monRationnel(1, 2);
  Rationnel<int> monRationnel2(1, 2);
  monRationnel2.diviser(monRationnel);
  ASSERT_THAT(monRationnel2.valeurApprox(), Eq(1.0));
}

// testing the divide function diviser with another rational
TEST(Rationnel, diviser3) {
  Rationnel<int> monRationnel(1, 1);
  Rationnel<int> monRationnel2(1, 2);
  monRationnel.diviser(monRationnel2);
  ASSERT_THAT(monRationnel.valeurApprox(), Eq(2.0));
}

// try to catch a null division error
TEST(Rationnel, diviserError1) {
  try {
    Rationnel<int> monRationnel(1, 1);
    Rationnel<int> monRationnel2(0, 2);
    monRationnel.diviser(monRationnel2);
    FAIL();
  } catch (const std::exception& e) {
    SUCCEED();
  }
}

// try to catch a null division error
TEST(Rationnel, diviserError1Entier) {
  try {
    Rationnel<Entier> monRationnel(Entier(1), Entier(1));
    Rationnel<Entier> monRationnel2(Entier(0), Entier(2));
    monRationnel.diviser(monRationnel2);
    FAIL();
  } catch (const std::exception& e) {
    SUCCEED();
  }
}

// testing the equality operator
TEST(Rationnel, equality1) {
  Rationnel<int> monRationnel(1, 2);
  Rationnel<int> monRationnel2(1, 2);
  ASSERT_TRUE(monRationnel2 == monRationnel);
}

// testing the equality operator
TEST(Rationnel, equality1Entier) {
  Rationnel<Entier> monRationnel(Entier(1), Entier(2));
  Rationnel<Entier> monRationnel2(Entier(1), Entier(2));
  ASSERT_TRUE(monRationnel2 == monRationnel);
}

// testing the equality operator
TEST(Rationnel, equality2) {
  Rationnel<int> monRationnel(1, 2);
  Rationnel<int> monRationnel2(2, 4);
  ASSERT_TRUE(monRationnel2 == monRationnel);
}

// testing addition
TEST(Rationnel, addition_op) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(2, 4);
  Rationnel<int> res = monRationnel + monRationnel2;
  ASSERT_THAT(res.valeurApprox(), Eq(0.75));
}

// testing substraction
TEST(Rationnel, substraction_op) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(2, 4);
  Rationnel<int> res = monRationnel - monRationnel2;
  ASSERT_THAT(res.valeurApprox(), Eq(-0.25));
}

// testing multiplication
TEST(Rationnel, multiplication_op) {
  Rationnel<int> monRationnel(1, 4);
  Rationnel<int> monRationnel2(4, 1);
  Rationnel<int> res = monRationnel * monRationnel2;
  ASSERT_THAT(res.valeurApprox(), Eq(1.0));
}

// testing division
TEST(Rationnel, division_op) {
  Rationnel<int> monRationnel(1, 5);
  Rationnel<int> monRationnel2(1, 5);
  Rationnel<int> res = monRationnel / monRationnel2;
  ASSERT_THAT(res.valeurApprox(), Eq(1.0));
}

TEST(Rationnel, arythmetic1) {
  Rationnel<Entier> A = Rationnel<Entier>(Entier(2003), Entier(1));
  Rationnel<Entier> B = Rationnel<Entier>(Entier(546932250), Entier(172125));
  Entier c1 = Entier(273375)*100000;
  Rationnel<Entier> C = Rationnel<Entier>(c1, Entier(23236875));
  Rationnel<Entier> Res = A - B + C;
  ASSERT_THAT(abs(Res.valeurApprox()-1.9411764705882888)<0.001, Eq(true));
}

}  // namespace

}  // namespace nombres
