#include "rationnel/rationnel.hpp"

#include <iostream>
#include <stdexcept>

#include "entier/entier.hpp"

namespace nombres {

// constructor
template <typename T>
Rationnel<T>::Rationnel(T num, T den) {
  // throw an error in case of a division per zero
  if (den == 0) {
    throw std::runtime_error("Rationnel::Rationnel: 0 division error");
  }
  _numerateur = num;
  _denominateur = den;
}

// destructor
template <typename T>
Rationnel<T>::~Rationnel() {
  // nothing to do here
}

// display the rational in a rational manner
template <typename T>
void Rationnel<T>::afficher() {
  std::cout << _numerateur << "/" << _denominateur << std::endl;
}

// return the float division between denominator and numerator
template <typename T>
double Rationnel<T>::valeurApprox() {
  // return 0.0 if numerator is null
  if(_numerateur==0 || _denominateur==0) {
    return 0.0;
  }
  // make a copy
  Rationnel<T> copy = Rationnel<T>(_numerateur, _denominateur);
  // we will divide the copy untill we got something small enough to be casted to flaot
  while(copy._numerateur>MAX_INT || copy._denominateur>MAX_INT) {
    copy._numerateur = Entier(copy._numerateur) / Entier(ENTIER_BASE);
    copy._denominateur = Entier(copy._denominateur) / Entier(ENTIER_BASE);
  }
  if(static_cast<float>(copy._denominateur)==0.0) { return 0.0; }
  return static_cast<float>(copy._numerateur) / static_cast<float>(copy._denominateur);
}

// greatest common diviser euclide algorithm
template <typename T>
T Rationnel<T>::_gcd(T a, T b) {
  if (a == 0) return b;
  return _gcd(b % a, a);
}

// transform the rational to the smallest num/den that are equal
template <typename T>
void Rationnel<T>::simplifie() {
  // cancel if numerator is zero
  if (_numerateur == 0) {
    return;
  }
  // if the greatest common diviser is not 1
  T greatest_common_div = _gcd(_numerateur, _denominateur);
  if (greatest_common_div != 1) {
    // divide by the greatest common divisor
    _numerateur = _numerateur / greatest_common_div;
    _denominateur = _denominateur / greatest_common_div;
  }
}

// add an integer
template <typename T>
void Rationnel<T>::ajouter(int n) {
  _numerateur += (_denominateur * n);
}

// add with another rational
template <typename T>
void Rationnel<T>::ajouter(const Rationnel<T>& r) {
  // if both denom are equal
  if (r.getDenom() == _denominateur) {
    // add numerator
    _numerateur += r.getNumer();
  } else {
    // else
    // save the previous denom
    T previous_denom = _denominateur;
    // multiply denom and numerator per the other operand's denom
    _denominateur = r.getDenom() * _denominateur;
    _numerateur = r.getDenom() * _numerateur;
    // add to the numerator the other operand times the previous denom
    _numerateur = _numerateur + (previous_denom * r.getNumer());
    // simplify
    // simplifie();
  }
}

// substract with an integer
template <typename T>
void Rationnel<T>::soustraire(int n) {
  _numerateur -= (_denominateur * n);
}

// substract with another rational
template <typename T>
void Rationnel<T>::soustraire(const Rationnel<T>& r) {
  // if both denom are equal
  if (r.getDenom() == _denominateur) {
    // substract numerator
    _numerateur -= r.getNumer();
  } else {
    // else
    // (it's the lazy implementation after which we simplify)
    // save the previous denom
    T previous_denom = _denominateur;
    // multiply denom and numerator per the other operand's denom
    _denominateur = r.getDenom() * _denominateur;
    _numerateur = r.getDenom() * _numerateur;
    // substract to the numerator the other operand times the previous denom
    _numerateur -= previous_denom * r.getNumer();
    // simplify
    //simplifie();
  }
}

// multiply with an integer
template <typename T>
void Rationnel<T>::multiplier(int n) {
  _numerateur = _numerateur * n;
}

// multiply with another rational
template <typename T>
void Rationnel<T>::multiplier(const Rationnel<T>& r) {
  _numerateur = r.getNumer() * _numerateur;
  _denominateur = r.getDenom() * _denominateur;
}

// divide with an integer
template <typename T>
void Rationnel<T>::diviser(int n) {
  // throw an error in case of a division per zero
  if (n == 0) {
    throw std::runtime_error("Rationnel::diviser: 0 division error");
  }
  // do the actual computation
  _denominateur = _denominateur * n;
}

// divide with another rational
template <typename T>
void Rationnel<T>::diviser(const Rationnel<T>& r) {
  // throw an error in case of a division per zero
  if (r.getNumer() == 0) {
    throw std::runtime_error("Rationnel::diviser: 0 division error");
  }
  // do the actual computation
  _numerateur = r.getDenom() * _numerateur;
  _denominateur = r.getNumer() * _denominateur;
}

// operator of equality between two rationals
template <typename T>
bool Rationnel<T>::operator==(const Rationnel<T>& left) {
  //
  // WARN: I don't know if relying on floating point precision
  //       and using valeurApprox() for comparison would be a
  //       good idea due to potential floating point precision
  //       problems. It is a common saying that it is not reliable
  //       for strict equality. So I'll play it safe even if unsure.
  //
  // create a copy to reduce em without affecting the original operands
  Rationnel<T> op1(left.getNumer(), left.getDenom());
  Rationnel<T> op2(_numerateur, _denominateur);
  // do the actual reducing
  op1.simplifie();
  op2.simplifie();
  // return the comparison now that it is relatively safe
  return (op1.getDenom() == op2.getDenom() && op2.getNumer() == op1.getNumer());
}

// acessors for num and denom
template <typename T>
T Rationnel<T>::getDenom() const {
  return _denominateur;
};
template <typename T>
T Rationnel<T>::getNumer() const {
  return _numerateur;
};

// operators
template <typename T>
Rationnel<T> Rationnel<T>::operator+(const Rationnel<T>& b) {
  Rationnel result(_numerateur, _denominateur);
  result.ajouter(b);
  return result;
}
template <typename T>
Rationnel<T> Rationnel<T>::operator*(const Rationnel<T>& b) {
  Rationnel result(_numerateur, _denominateur);
  result.multiplier(b);
  return result;
}
template <typename T>
Rationnel<T> Rationnel<T>::operator/(const Rationnel<T>& b) {
  // throw an excpetion if user try to divide by 0
  Rationnel result(_numerateur, _denominateur);
  result.diviser(b);
  return result;
}
template <typename T>
Rationnel<T> Rationnel<T>::operator-(const Rationnel<T>& b) {
  Rationnel result(_numerateur, _denominateur);
  result.soustraire(b);
  return result;
}

template class Rationnel<int>;
template class Rationnel<nombres::Entier>;

}  // namespace nombres