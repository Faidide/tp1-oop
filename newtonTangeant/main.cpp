#include <iostream>

#include "suites/suite.hpp"

// Vn iteration function
float a = 2.0f;
float vnIteration(std::vector<float> terms) {
  return (terms[0] / 2.0f) + (a / (2.0f * terms[0]));
}

using namespace suites;

int main() {
  /* COMPUTE NEWTON TANGEANT SEQUENCE FOR a=2*/
  // the initial terms of the sequence
  std::vector<float> initialTerms;
  // add the two inital terms of fibonacci sequence
  initialTerms.push_back(1.0);
  // create the suite object
  Suite<float> maSuite(initialTerms, vnIteration);
  try {
    // get the 4th term
    float resultat = maSuite.computeUntill(100);
    // test the result is close to what is expected
    std::cout << "Approximation with newton tangeant 100th term a=" << a << " :"
              << resultat << std::endl;
  } catch (const std::exception& e) {
    // make the test fail with the error message
    std::cerr << "Error in sequence:" << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  /* COMPUTE NEWTON TANGEANT SEQUENCE FOR a=2*/
  // the initial terms of the sequence
  a = 4.0f;
  try {
    // get the 4th term
    float resultat = maSuite.computeUntill(100);
    // test the result is close to what is expected
    std::cout << "Approximation with newton tangeant 100th term a=" << a << " :"
              << resultat << std::endl;
  } catch (const std::exception& e) {
    // make the test fail with the error message
    std::cerr << "Error in sequence:" << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  /* COMPUTE NEWTON TANGEANT SEQUENCE FOR a=2*/
  // the initial terms of the sequence
  a = 9.0f;
  try {
    // get the 4th term
    float resultat = maSuite.computeUntill(100);
    // test the result is close to what is expected
    std::cout << "Approximation with newton tangeant 100th term a=" << a << " :"
              << resultat << std::endl;
  } catch (const std::exception& e) {
    // make the test fail with the error message
    std::cerr << "Error in sequence:" << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  /* COMPUTE NEWTON TANGEANT SEQUENCE FOR a=2*/
  // the initial terms of the sequence
  a = 25.0f;
  try {
    // get the 4th term
    float resultat = maSuite.computeUntill(100);
    // test the result is close to what is expected
    std::cout << "Approximation with newton tangeant 100th term a=" << a << " :"
              << resultat << std::endl;
  } catch (const std::exception& e) {
    // make the test fail with the error message
    std::cerr << "Error in sequence:" << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
